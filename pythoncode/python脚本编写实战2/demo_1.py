class Animal(object):
    def __init__(self,name:str='wudi', colour:str='yellow', age:int= 0, sex:str='man'):
        self.name =name
        self.colour = colour
        self.age = age
        self.sex = sex

    def cry(self):
        '''
        定义一个会叫的函数
        :return: None
        '''
        print('会叫')

    def run(self):
        '''
        定义一个会跑的函数
        :return: None
        '''
        print('会跑')



class cat(Animal):
    def __init__(self,name:str='wudi', colour:str='yellow', age:int= 0, sex:str='man',hair:str="短毛"):
        super(cat,self).__init__(name,colour,age,sex)
        self.hair = hair

    def catchTheMouse(self):
        '''
        定义一个会抓老鼠的函数
        :return: None
        '''
        print('会抓老鼠')

    def cry(self):
        '''
        重写父类Animal中的cry方法
        :return: None
        '''
        print('喵喵叫')



class dog(Animal):
    def __init__(self,name:str='wudi', colour:str='yellow', age:int= 0, sex:str='man',hair:str="长毛"):
        super(dog,self).__init__(name,colour,age,sex)
        self.hair = hair

    def lookHouse(self):
        '''
        定义一个函数 看家
        :return: None
        '''
        print('会看家')

    def cry(self):
        '''
        重写父类Animal中的 cry方法
        :return: None
        '''
        print('汪汪叫')


if __name__ == "__main__":
    cat_1 = cat()
    cat_1.catchTheMouse()
    print(f"name:{cat_1.name} \nage:{cat_1.age} \ncolour:{cat_1.colour} \nsex:{cat_1.sex} \nhair:{cat_1.hair} \n{'抓到了老鼠'}")

    dog_1 = dog()
    dog_1.lookHouse()
    print(f"name:{dog_1.name} \nage:{dog_1.age} \ncolour:{dog_1.colour} \nsex:{dog_1.sex} \nhair:{dog_1.hair}")

